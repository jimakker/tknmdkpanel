const axios = require("axios")
const crypto = require("crypto")
const qs = require("querystring")
const path = require('path')

const API_URL = "https://ws.audioscrobbler.com/2.0/"

const API_KEY = process.env.API_KEY
const API_SECRET = process.env.API_SECRET
const SESSION_KEY = process.env.SESSION_KEY

axios.defaults.headers.common['User-Agent'] = 'tknmdk-scrobbler';

function md5(string) {
  return crypto
    .createHash("md5")
    .update(string, "utf8")
    .digest("hex")
}

function timestamp() {
  return Math.floor(Date.now() / 1000)
}

async function getToken(user, pass) {
  var authToken = md5(user + md5(pass))
  var sig =
    "api_key" +
    API_KEY +
    "authToken" +
    authToken +
    "methodauth.getMobileSessionusername" +
    user +
    API_SECRET
  let apiSig = md5(sig)
  try {
    let { data } = await axios.request({
      method: "POST",
      url: API_URL,
      params: {
        method: "auth.getMobileSession",
        username: user,
        authToken: authToken,
        api_key: API_KEY,
        api_sig: apiSig,
        format: "json"
      }
    })

    console.log(data)
  } catch (error) {
    console.error(error)
  }
}

function doScrobble(options) {
  var sig =
    "api_key" +
    API_KEY +
    "artist" +
    options.artist +
    "method" +
    options.method +
    "sk" +
    SESSION_KEY +
    (options.tags != null ? "tags" + options.tags : "") +
    "timestamp" +
    options.timestamp +
    "track" +
    options.track +
    API_SECRET
  let apiSig = md5(sig)

  let postData = {
    method: "track.scrobble",
    api_key: API_KEY,
    api_sig: apiSig,
    format: "json",
    artist: options.artist,
    track: options.track,
    sk: SESSION_KEY,
    timestamp: options.timestamp
  }

  if (options.tags) {
    postData.tags = options.tags
  }

  return new Promise(async (resolve, reject) => {
    try {
      const config = {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        }
      }
  
      let { data } = await axios.post(API_URL, qs.stringify(postData), config)
      return resolve(data)
    } catch (error) {
      console.error(error)
      return reject(error)
    }
  })
}


const fastify = require("fastify")({
  logger: false
})

fastify.register(require('fastify-static'), {
    root: path.join(__dirname, 'public'),
    prefix: '/',
})

fastify.get("/api/hello", (request, reply) => {
  reply.send({ hello: "world" })
})

fastify.post("/api/scrobble", async (request, reply) => {
  let scrobbleOptions = {
    artist: request.body.artist,
    track: request.body.track,
    method: "track.scrobble",
    tags: null,
    timestamp: timestamp()
  }
  try {
    await doScrobble(scrobbleOptions)
    reply.send({ ok: true })
  } catch (error) {
    reply.code(500).send({ok: false, error: error.toString()})    
  }
})

fastify.get("/api/artist/:artist", async (request, reply) => {
  let promises = [
    axios.get(API_URL, {
      params: {
        method: 'artist.getInfo',
        artist: request.params.artist,
        api_key: API_KEY,
        format: 'json'
      }
    }),
    axios.get(API_URL, {
      params: {
        method: 'artist.getTopAlbums',
        artist: request.params.artist,
        api_key: API_KEY,
        format: 'json'
      }
    }),
    axios.get(API_URL, {
      params: {
        method: 'artist.getTopTracks',
        artist: request.params.artist,
        api_key: API_KEY,
        format: 'json'
      }
    }),
    axios.get(API_URL, {
      params: {
        method: 'artist.getSimilar',
        artist: request.params.artist,
        api_key: API_KEY,
        format: 'json'
      }
    })
  ]

  try {
    let res = await Promise.all(promises)

    let data = {}

    res.forEach(el => {
      data[Object.keys(el.data)[0]] = el.data[Object.keys(el.data)[0]]
    })
    
    reply.send(data)
  } catch (error) {
    console.error(error)
    reply.code(500).send({ok: false})
  }
})

fastify.get("/api/artist/:artist/track/:track", async (request, reply) => {
  let promises = [
    axios.get(API_URL, {
      params: {
        method: 'track.getInfo',
        artist: request.params.artist,
        track: request.params.track,
        api_key: API_KEY,
        format: 'json'
      }
    }),
    axios.get(API_URL, {
      params: {
        method: 'track.getSimilar',
        artist: request.params.artist,
        track: request.params.track,
        api_key: API_KEY,
        format: 'json'
      }
    }),
    axios.get(API_URL, {
      params: {
        method: 'track.getTopTags',
        artist: request.params.artist,
        track: request.params.track,
        api_key: API_KEY,
        format: 'json'
      }
    })
  ]
  try {
    let res = await Promise.all(promises)

    let data = {}

    res.forEach(el => {
      data[Object.keys(el.data)[0]] = el.data[Object.keys(el.data)[0]]
    })
    
    reply.send(data)
  } catch (error) {
    console.error(error)
    reply.code(500).send({ok: false})
  }
})

fastify.get("/api/artist/:artist/album/:album", async (request, reply) => {
  try {
    let {data} = await axios.get(API_URL, {
      params: {
        method: 'album.getInfo',
        artist: request.params.artist,
        album: request.params.album,
        api_key: API_KEY,
        format: 'json'
      }
    })
    
    reply.send(data)
  } catch (error) {
    console.error(error)
    reply.code(500).send({ok: false})
  }
})

fastify.get("/api/tag/:tag", async (request, reply) => {
  try {
    let promises = [
      axios.get(API_URL, {
        params: {
          method: 'tag.getInfo',
          tag: request.params.tag,
          api_key: API_KEY,
          format: 'json'
        }
      }),
      axios.get(API_URL, {
        params: {
          method: 'tag.getSimilar',
          tag: request.params.tag,
          api_key: API_KEY,
          format: 'json'
        }
      }),
      axios.get(API_URL, {
        params: {
          method: 'tag.getTopAlbums',
          tag: request.params.tag,
          api_key: API_KEY,
          format: 'json'
        }
      }),
      axios.get(API_URL, {
        params: {
          method: 'tag.getTopArtists',
          tag: request.params.tag,
          api_key: API_KEY,
          format: 'json'
        }
      }),
      axios.get(API_URL, {
        params: {
          method: 'tag.getTopTracks',
          tag: request.params.tag,
          api_key: API_KEY,
          format: 'json'
        }
      }),
      // axios.get(API_URL, {
      //   params: {
      //     method: 'tag.getWeeklyChartList',
      //     tag: request.params.tag,
      //     api_key: API_KEY,
      //     format: 'json'
      //   }
      // })
    ]

    let res = await Promise.all(promises)

    let data = {}

    res.forEach(el => {
      data[Object.keys(el.data)[0]] = el.data[Object.keys(el.data)[0]]
    })

    reply.send(data)
  } catch (error) {
    console.error(error)
    reply.code(500).send({ok: false})
  }
})

fastify.listen(3000, '0.0.0.0', (err, address) => {
  if (err) throw err
  fastify.log.info(`server listening on ${address}`)
})
