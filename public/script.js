console.log("TKNMDKv2020")

let bus = document.getElementById('bus')

function _get(url) {
  return new Promise(async (resolve, reject) => {
    try {
      let response = await fetch(url, {
        method: "GET",
        headers: {
          // "Content-Type": "application/json"
        },
      })

      let data = await response.json()

      return resolve(data)
    } catch (error) {
      return reject(error)
    }
  })
}

function _scrobble(track) {
  return new Promise(async (resolve, reject) => {
    try {
      let response = await fetch("/api/scrobble", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(track)
      })

      let data = await response.json()

      return resolve(data)
    } catch (error) {
      return reject(error)
    }
  })
}

function scrobblerState() {
  return {
    doScrobble: false,
    scrobbleData: {
      artist: "",
      track: ""
    },
    async scrobble() {
      let track = {
        artist: this.scrobbleData.artist,
        track: this.scrobbleData.track,
        t: Date.now()
      }

      try {
        if (this.doScrobble) {
          await _scrobble(track)
        }
        this.dispatchTrack(track)
        this.clearInputs()
      } catch (error) {
        console.error(error)
        window.alert("Error while scrobbling :(")
      }
    },
    clearInputs() {
      this.scrobbleData.artist = ""
      this.scrobbleData.track = ""
    },
    dispatchTrack(track) {
      let event = new CustomEvent('scrobble', {
        detail: track
      })
      bus.dispatchEvent(event)
    }
  }
}

function trackListState() {
  let tracklist = localStorage.getItem("tracklist")
  let tracklists = localStorage.getItem("tracklists")
  if (tracklist) {
    tracklist = JSON.parse(tracklist)
  } else {
    tracklist = {
      name: '',
      tracks: [],
    }
  }

  if (tracklists) {
    tracklists = JSON.parse(tracklists)
  } else {
    tracklists = []
  }

  if (window.location.hash.length > 1) {
    if (tracklist.tracks.length) {
      if (window.confirm('Import tracklist from url hash?')) {
        let hex = window.location.hash.replace('#', '')

        try {
          let raw = atob(hex)
          tracklist = JSON.parse(raw)
          localStorage.setItem('tracklist', JSON.stringify(tracklist))
          window.location.hash = ''
        } catch (error) {
          console.error(error)
          alert('Cant import tracklist :(')
        }
      }
    }
  }

  return {
    tracklist: tracklist,
    tracklists: tracklists,
    rawTracklist: '',
    tracklistToLoad: '',
    initBus() {
      bus.addEventListener("scrobble", (e) => { 
        this.addToTrackList(e.detail)
      });
    },
    addToTrackList(track) {
      track.o = this.tracklist.tracks.length
      this.tracklist.tracks.push(track)
      this.saveCurrentTracklist()
    },
    saveCurrentTracklist() {
      localStorage.setItem('tracklist', JSON.stringify(this.tracklist))
    },
    clearCurrentTracklist() {
      if (window.confirm('Clear TrackList?')) {
        localStorage.removeItem('tracklist')
        this.tracklist = {
          name: '',
          tracks: [],
        }
      }
    },
    saveTrackList() {
      let existingIdx = this.tracklists.findIndex(t => t.name === this.tracklist.name)
      if (existingIdx !== -1) {
        if (confirm('there is already a tracklist with that name, overwrite?')) {
          this.tracklists[existingIdx] = this.tracklist
        }
      } else {
        this.tracklists.push(this.tracklist)
      }
      localStorage.setItem('tracklists', JSON.stringify(this.tracklists))
    },
    handleClick(type, data) {
      let event = new CustomEvent('select:' + type, {
        detail: data
      })
      bus.dispatchEvent(event)
    },
    deleteTrack(i) {
      this.tracklist.tracks.splice(i, 1)
      this.tracklist.tracks.forEach((element, i) => {
        element.o = i
      })
      this.saveCurrentTracklist()
    },
    moveTrack(direction, i) {
      if (direction === 'up' && i !== 0) {
        let _tmp = this.tracklist.tracks[i - 1]
        _tmp.o = i
        this.tracklist.tracks[i - 1] = this.tracklist.tracks[i]
        this.tracklist.tracks[i - 1].o = i - 1
        this.tracklist.tracks[i] = _tmp
      } else if (direction === 'down' && i < (this.tracklist.tracks.length - 1)) {
        let _tmp = this.tracklist.tracks[i + 1]
        _tmp.o = i
        this.tracklist.tracks[i + 1] = this.tracklist.tracks[i]
        this.tracklist.tracks[i + 1].o = i + 1
        this.tracklist.tracks[i] = _tmp
      }
      this.saveCurrentTracklist()
    },
    importTracklist() {
      try {
        let tracklist = JSON.parse(this.rawTracklist)
        this.tracklist = tracklist
        this.saveCurrentTracklist()
        this.rawTracklist = ''
      } catch (error) {
        alert("Can't import playlist :(")
      }
    },
    loadTrackList() {
      if (this.tracklistToLoad) {
        for (let index = 0; index < this.tracklists.length; index++) {
          const element = this.tracklists[index];
          if (element.name === this.tracklistToLoad) {
            this.tracklist = JSON.parse(JSON.stringify(element))
            this.saveCurrentTracklist()
            break
          }
        }
      }
    },
    deleteTrackList() {
      if (!this.tracklistToLoad) {
        return
      }

      if (window.confirm('Delete tracklist ' + this.tracklistToLoad + '?')) {
        let _index = -1
        for (let index = 0; index < this.tracklists.length; index++) {
          const element = this.tracklists[index];
          if (element.name === this.tracklistToLoad) {
            _index = index
            break
          }
        }
        if (_index !== -1) {
          this.tracklists.splice(_index, 1)
          localStorage.setItem('tracklists', JSON.stringify(this.tracklists))
        }
      }
    }
  }
}

function infoState() {
  return {
    selectedArtist: '',
    selectedTrack: '',
    selectedAlbum: '',
    selectedTag: '',
    isSearching: false,
    tabs: [],
    currentTab: {},
    init() {
      bus.addEventListener("select:artist", (e) => { 
        this.search(e.detail)
      });
      bus.addEventListener("select:track", (e) => { 
        this.search(e.detail)
      });
    },
    search(args = {}) {
      if (this.isSearching) {
        return
      }

      if (Object.keys(args).length) {
        this.selectedArtist = ''
        this.selectedTrack = ''
        this.selectedAlbum = ''
        this.selectedTag = ''
        if (args.artist) {
          this.selectedArtist = args.artist
        }
        if (args.track) {
          this.selectedTrack = args.track
        }
        if (args.album) {
          this.selectedAlbum = args.album
        }
        if (args.tag) {
          this.selectedTag = args.tag
        }
      }

      if (this.selectedArtist && this.selectedTrack) {
        this.getTrackInfo()
      } else if (this.selectedArtist && this.selectedAlbum) {
        this.getAlbumInfo()
      } else if (this.selectedArtist) {
        this.getArtistInfo()
      } else if(this.selectedTag) {
        this.getTagInfo()
      }
    },
    async getArtistInfo() {
      this.isSearching = true
      let info = await _get('/api/artist/' + this.selectedArtist)
      this.isSearching = false
      let tab = {
        name: 'Artist: ' + this.selectedArtist,
        type: 'artist',
        data: info,
        showMore: false,
      }
      this.tabs.push(tab)
      this.selectTab(tab.name)
    },
    async getTrackInfo() {
      this.isSearching = true
      let info = await _get('/api/artist/' + this.selectedArtist + '/track/' + this.selectedTrack)
      this.isSearching = false
      let tab = {
        name: 'Track: ' + this.selectedTrack,
        type: 'track',
        data: info,
        showMore: false,
      }
      this.tabs.push(tab)
      this.selectTab(tab.name)
    },
    async getAlbumInfo() {
      this.isSearching = true
      let info = await _get('/api/artist/' + this.selectedArtist + '/album/' + this.selectedAlbum)
      this.isSearching = false
      let tab = {
        name: 'Album: ' + this.selectedAlbum,
        type: 'album',
        data: info.album,
        showMore: false,
      }
      this.tabs.push(tab)
      this.selectTab(tab.name)
    },
    async getTagInfo() {
      this.isSearching = true
      let info = await _get('/api/tag/' + this.selectedTag)
      this.isSearching = false
      let tab = {
        name: 'Tag: ' + this.selectedTag,
        type: 'tag',
        data: info,
        showMore: false,
      }
      this.tabs.push(tab)
      this.selectTab(tab.name)
    },
    selectTab(tabName) {
      for (let index = 0; index < this.tabs.length; index++) {
        const element = this.tabs[index];
        if (element.name === tabName) {
          this.currentTab = element
          break
        }
      }
    },
    closeTab(tabName) {
      this.currentTab = {}
      let _index
      for (let index = 0; index < this.tabs.length; index++) {
        const element = this.tabs[index];
        if (element.name === tabName) {
          _index = index
          break
        }
      }
      this.tabs.splice(_index, 1)
    },
    paginate(items, showMore) {
      return items.slice(0, showMore ? 100 : 20)
    }
  }
}
