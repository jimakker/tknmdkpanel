FROM node:lts-alpine

ADD . /app

WORKDIR /app

RUN npm i

ENTRYPOINT node server.js